﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.IO;
using Microsoft.Win32;
using System.Windows.Input;

namespace Coursework
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Passport> collDevice = new ObservableCollection<Passport>();
        string _fileName;

        public MainWindow()
        {
            InitializeComponent();    
        }
        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(collDevice.GetType());
            if (string.IsNullOrEmpty(_fileName))
            {
                SaveAsButton_Click(sender, e);
            }
            try
            {
                using (FileStream fs = new FileStream(_fileName, FileMode.Create))
                {
                    x.Serialize(fs, collDevice);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void clearAllButton_Click(object sender, RoutedEventArgs e)
        {
            collDevice.Clear();
        }
        private void toCalibrationTableButton_Click(object sender, RoutedEventArgs e)
        {
            calibrationList.Items.Clear();
            InitListView();
        }
        private void quitButton_Click(object sender, RoutedEventArgs e)
        {
            System.Environment.Exit(0);
        }
        private void MyGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = e.Row.GetIndex() + 1;
        }

        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "All files (*.*)|*.*|XML files (*.xml)|*.xml";
            openFileDialog.FilterIndex = 2;
            
            if (openFileDialog.ShowDialog() != null)
            {
                try
                {
                    System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(collDevice.GetType());

                    using (FileStream fs = new FileStream(openFileDialog.FileName, FileMode.Open))
                    {
                        try
                        {
                            collDevice = (ObservableCollection<Passport>)x.Deserialize(fs);
                        }
                        catch (InvalidOperationException ex)
                        {
                            MessageBox.Show("Открываемый файл повреждён или содержит неверные значения! Подробности: " + ex.Message);
                        }
                        dataGridPassports.ItemsSource = collDevice;
                    }
                }
                catch (FileNotFoundException ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. " + ex.Message);
                }
                catch (FileLoadException ex)
                {
                    MessageBox.Show(ex.Message);
                    Environment.Exit(0);
                }
                catch (IOException ex)
                {
                    MessageBox.Show(ex.Message);
                    Environment.Exit(0);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }

            }
            _fileName = openFileDialog.FileName;
        }

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            dataGridPassports.ItemsSource = collDevice;
        }

        private void SaveAsButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "XML Files (*.xml)|*.xml";

            if (sfd.ShowDialog() != null)
            {
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(collDevice.GetType());
                try
                {
                    using (FileStream fs = new FileStream(sfd.FileName, FileMode.CreateNew))
                    {
                        x.Serialize(fs, collDevice);
                    }
                }
                catch (FileNotFoundException ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. " + ex.Message);
                }
                catch (FileLoadException ex)
                {
                    MessageBox.Show(ex.Message);
                    Environment.Exit(0);
                }
                catch (IOException ex)
                {
                    MessageBox.Show(ex.Message);
                    Environment.Exit(0);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            _fileName = sfd.FileName;
        }
        public void InitListView()
        {
            for (int i = 0; i < collDevice.Count; i++)
            {
                CalibrationGraph collGraphItem = new CalibrationGraph
                {
                    DeviceNameAndIndentification = collDevice[i].GetName(),
                    CalibrationYearAndPeriodicity = collDevice[i].CalibratDate + "/" + collDevice[i].Periodicity,
                    AreaMeasurementsCode = collDevice[i].AreaCode,
                    LocationUse = collDevice[i].LocationOfUse,
                    JanuaryColumn = collDevice[i].January,
                    FebruaryColumn = collDevice[i].February,
                    MarchColumn = collDevice[i].March,
                    AprilColumn = collDevice[i].April,
                    MayColumn = collDevice[i].May,
                    JuneColumn = collDevice[i].June,
                    JulyColumn = collDevice[i].July,
                    AugustColumn = collDevice[i].August,
                    SeptemberColumn = collDevice[i].September,
                    OctoberColumn = collDevice[i].October,
                    NovemberColumn = collDevice[i].November,
                    DecemberColumn = collDevice[i].December,
                    GeneralCount = collDevice[i].January +
                    collDevice[i].February +
                    collDevice[i].March +
                    collDevice[i].April +
                    collDevice[i].May +
                    collDevice[i].June +
                    collDevice[i].July +
                    collDevice[i].August +
                    collDevice[i].September +
                    collDevice[i].October +
                    collDevice[i].November +
                    collDevice[i].December
                };
                calibrationList.Items.Add(collGraphItem);
            }
        }
        SelectionGraph AddToSelectionGraph(int ind)
        {
                SelectionGraph selGraph = new SelectionGraph()
                {
                    SelName = collDevice[ind].GetName(),
                    CodeArea = collDevice[ind].AreaCode,
                    CalibYear = collDevice[ind].CalibratDate,
                    SelPeriod = collDevice[ind].Periodicity,
                };
                selList.Items.Add(selGraph);
                return selGraph;
        }
        private void ChoiseButton_Click(object sender, RoutedEventArgs e)
        {
            selList.Items.Clear();
            int year;
            int.TryParse(textBoxYear1.Text, out year);
            int index = comboBoxMonth1.SelectedIndex;
            SelectionGraph sel;
                for (int i = 0; i < collDevice.Count; i++)
                {
                    if (collDevice[i].January != 0 && index == 0 && collDevice[i].CalibratDate == year)
                    {
                        sel = AddToSelectionGraph(i);
                        sel.CountOfDevicesPerMonth = collDevice[i].January;
                        break;
                    }
                    if (collDevice[i].February != 0 && index == 1 && collDevice[i].CalibratDate == year)
                    {
                        sel = AddToSelectionGraph(i);
                        sel.CountOfDevicesPerMonth = collDevice[i].February;
                        break;
                    }
                    if (collDevice[i].March != 0 && index == 2 && collDevice[i].CalibratDate == year)
                    {
                        sel = AddToSelectionGraph(i);
                        sel.CountOfDevicesPerMonth = collDevice[i].March;
                        break;
                    }
                    if (collDevice[i].April != 0 && index == 3 && collDevice[i].CalibratDate == year)
                    {
                        sel = AddToSelectionGraph(i);
                        sel.CountOfDevicesPerMonth = collDevice[i].April;
                        break;
                    }
                    if (collDevice[i].May != 0 && index == 4 && collDevice[i].CalibratDate == year)
                    {
                        sel = AddToSelectionGraph(i);
                        sel.CountOfDevicesPerMonth = collDevice[i].May;
                        break;
                    }
                    if (collDevice[i].June != 0 && index == 5 && collDevice[i].CalibratDate == year)
                    {
                        sel = AddToSelectionGraph(i);
                        sel.CountOfDevicesPerMonth = collDevice[i].June;
                        break;
                    }
                    if (collDevice[i].July != 0 && index == 6 && collDevice[i].CalibratDate == year)
                    {
                        sel = AddToSelectionGraph(i);
                        sel.CountOfDevicesPerMonth = collDevice[i].July;
                        break;
                    }
                    if (collDevice[i].August != 0 && index == 7 && collDevice[i].CalibratDate == year)
                    {
                        sel = AddToSelectionGraph(i);
                        sel.CountOfDevicesPerMonth = collDevice[i].August;
                        break;
                    }
                    if (collDevice[i].September != 0 && index == 8 && collDevice[i].CalibratDate == year)
                    {
                        sel = AddToSelectionGraph(i);
                        sel.CountOfDevicesPerMonth = collDevice[i].September;
                        break;
                    }
                    if (collDevice[i].October != 0 && index == 9 && collDevice[i].CalibratDate == year)
                    {
                        sel = AddToSelectionGraph(i);
                        sel.CountOfDevicesPerMonth = collDevice[i].October;
                        break;
                    }
                    if (collDevice[i].November != 0 && index == 10 && collDevice[i].CalibratDate == year)
                    {
                        sel = AddToSelectionGraph(i);
                        sel.CountOfDevicesPerMonth = collDevice[i].November;
                        break;
                    }
                    if (collDevice[i].December != 0 && index == 11 && collDevice[i].CalibratDate == year)
                    {
                        sel = AddToSelectionGraph(i);
                        sel.CountOfDevicesPerMonth = collDevice[i].December;
                        break;
                    }
                }
            } 
        
 
        void textBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!Char.IsDigit(e.Text, 0) && !Char.IsWhiteSpace(e.Text, 0))
            {
                e.Handled = true;
            }
        }
    }
}