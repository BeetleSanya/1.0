﻿using System;

namespace Coursework
{
    public class CalibrationGraph
    {
        string _deviceNameAndIndent;
        int _measurementsCode;
        string _calibrationYearAndPeriod;
        string _locatOfUse;

        public string DeviceNameAndIndentification //Наименование
        {
            get
            {
                return _deviceNameAndIndent;
            }
            set
            {
                _deviceNameAndIndent = value;
            }
        }
        public int AreaMeasurementsCode//Код области измерений
        {
            get
            {
                return _measurementsCode;
            }
            set
            {
                try
                {
                    _measurementsCode = value;
                }
                catch(FormatException)
                {
                    _measurementsCode = default(int);
                }
                if (value < 0)
                {
                    _measurementsCode = Math.Abs(value);
                }
            }
        }
        public string LocationUse//Место эксплуатации
        {
            get
            {
                return _locatOfUse;
            }
            set
            {
                _locatOfUse = value;
            }
        }
        public string CalibrationYearAndPeriodicity// Год и периодичность поверки
        {
            get
            {
                return _calibrationYearAndPeriod;
            }
            set
            {
                _calibrationYearAndPeriod = value;
            }
        }
//month
        int _january;
        int _february;
        int _march;
        int _april;
        int _may;
        int _june;
        int _july;
        int _august;
        int _september;
        int _october;
        int _november;
        int _december;
        int _general;
        public int JanuaryColumn //Кол-во приборов в Январе
        {
            get { return _january; }
            set
            {
                try
                {
                    _january = value;
                }
                catch (FormatException)
                {
                    _january = default(int);
                }
                if (value < 0)
                {
                    _january = Math.Abs(value);
                }
            }
        }
        public int FebruaryColumn//Кол-во приборов в феврале
        {
            get { return _february; }
            set
            {
                try
                {
                    _february = value;
                }
                catch (FormatException)
                {
                    _february = default(int);
                }
                if (value < 0)
                {
                    _february = Math.Abs(value);
                }
            }
        }
        public int MarchColumn//Кол-во приборов в марте
        {
            get { return _march; }
            set
            {
                try
                {
                    _march = value;
                }
                catch (FormatException)
                {
                    _march = default(int);
                }
                if (value < 0)
                {
                    _march = Math.Abs(value);
                }
            }
        }
        public int AprilColumn//Кол-во приборов в апреле
        {
            get { return _april; }
            set
            {
                try
                {
                    _april = value;
                }
                catch (FormatException)
                {
                    _april = default(int);
                }
                if (value < 0)
                {
                    _april = Math.Abs(value);
                }
            }
        }
        public int MayColumn//Кол-во приборов в мае
        {
            get { return _may; }
            set
            {
                try
                {
                    _may = value;
                }
                catch (FormatException)
                {
                    _may = default(int);
                }
                if (value < 0)
                {
                    _may = Math.Abs(value);
                }
            }
        }
        public int JuneColumn//Кол-во приборов в июне
        { get { return _june; }
            set
            {
                try
                {
                    _june = value;
                }
                catch (FormatException)
                {
                    _june = default(int);
                }
                if (value < 0)
                {
                    _june = Math.Abs(value);
                }
            } 
        }
        public int JulyColumn//Кол-во приборов в июле
        {
            get { return _july; }
            set
            {
                try
                {
                    _july = value;
                }
                catch (FormatException)
                {
                    _july = default(int);
                }
                if (value < 0)
                {
                    _july = Math.Abs(value);
                }
            }
        }
        public int AugustColumn//Кол-во приборов в августе
        { 
            get { return _august; }
            set
            {
                try
                {
                    _august = value;
                }
                catch (FormatException)
                {
                    _august = default(int);
                }
                if (value < 0)
                {
                    _august = Math.Abs(value);
                }
            } 
        }
        public int SeptemberColumn//Кол-во приборов в сентябре
        {
            get { return _september; }
            set
            {
                try
                {
                    _september = value;
                }
                catch (FormatException)
                {
                    _september = default(int);
                }
                if (value < 0)
                {
                    _september = Math.Abs(value);
                }
            }
        }
        public int OctoberColumn//Кол-во приборов в октябре
        {
            get { return _october; }
            set
            {
                try
                {
                    _october = value;
                }
                catch (FormatException)
                {
                    _october = default(int);
                }
                if (value < 0)
                {
                    _october = Math.Abs(value);
                }
            } 
        }
        public int NovemberColumn//Кол-во приборов в ноябре
        { 
            get { return _november; }
            set
            {
                try
                {
                    _november = value;
                }
                catch (FormatException)
                {
                    _november = default(int);
                }
                if (value < 0)
                {
                    _november = Math.Abs(value);
                }
            }
        }
        public int DecemberColumn//Кол-во приборов в декабре
        { 
            get { return _december; }
            set
            {
                try
                {
                    _december = value;
                }
                catch (FormatException)
                {
                    _december = default(int);
                }
                if (value < 0)
                {
                    _december = Math.Abs(value);
                }
            }
        }
        public int GeneralCount//Общее кол-во приборов
        {
            get { return _general; }
            set
            {
                try
                {
                    _general = value;
                }
                catch (FormatException)
                {
                    _general = default(int);
                }
                if (value < 0)
                {
                    _general = Math.Abs(value);
                }
            }
        }
    }
}