﻿using System;
using System.Windows;

namespace Coursework
{
    public class Passport
    {
        int _areaCode;
        string _serialNumber;
        int _periodicity;
        int _date;
        double _accuracy;
        double _lowLim;
        double _upLim;
        string _deviceName;
        string _locOfUse;

        public int AreaCode //Код области измерения
        {
            get { return _areaCode; }
            set
            {
                try
                {
                    _areaCode = value;
                }
                catch (FormatException)
                {
                    _areaCode = default(int);
                }
                if (value < 0)
                {
                    _areaCode = Math.Abs(value);
                }
                if ((value > 25 && value < 99) || value>99)
                {
                    _areaCode = default(int);
                }
            }
        }                                                                                                                                                                                   
        public string SerialNumber //Заводской номер
        {
            get
            {
                return _serialNumber;
            }
            set
            {
                _serialNumber = value;
            }
        }
        public string DeviceName //Наименование
        {
            get
            {
                return _deviceName;
            }
            set
            {
                _deviceName = value;
            }
        }
        public string LocationOfUse //Место эксплуатации
        {
            get
            {
                return _locOfUse;
            }
            set
            {
                _locOfUse = value;
            }
        }
        public double Accuracy //Точность
        {
            get { return _accuracy; }
            set
            {
                try
                {
                    _accuracy = value;
                }
                catch (FormatException)
                {
                    _accuracy = default(int);
                }
                if (value < 0)
                {
                    _accuracy = Math.Abs(value);
                }
            }
        }
        public double LowerLimitOfMeasurement //Нижний предел измерений
        {
            get { return _lowLim; }
            set
            {
                try
                {
                    _lowLim = value;
                }
                catch (FormatException)
                {
                    _lowLim = default(int);
                }
            }
        }
        public double UpperLimitOfMeasurement //Верхний предел измерений
        {
            get { return _upLim; }
            set
            {
                try
                {
                    _upLim = value;
                }
                catch (FormatException)
                {
                    _upLim = default(int);
                }
            }
        }
        public int Periodicity //Периодичность
        {
            get
            {
                return _periodicity;
            }
            set
            {
                try
                {
                    _periodicity = value;
                }
                catch (FormatException)
                {
                    _periodicity = default(int);
                }
                if (value < 0)
                {
                    _periodicity = Math.Abs(value);
                }
            }
        }
        public int CalibratDate //Год поверки
        {
            get
            {
                return _date;
            }
            set
            {
                try
                {
                    _date = value;
                }
                catch (FormatException)
                {
                    _date = default(int);
                }
                if (value < 0)
                {
                    _date = Math.Abs(value);
                }
            }
        }
        //months
        int _january;
        int _february;
        int _march;
        int _april;
        int _may;
        int _june;
        int _july;
        int _august;
        int _september;
        int _october;
        int _november;
        int _december;

        public int January
        {
            get { return _january; }
            set
            {
                try
                {
                    _january = value;
                }
                catch (FormatException)
                {
                    _january = default(int);
                }
                if (value < 0)
                {
                    _january = Math.Abs(value);
                }
            }
        }
        public int February
        {
            get { return _february; }
            set
            {
                try
                {
                    _february = value;
                }
                catch (FormatException)
                {
                    _february = default(int);
                }
                if (value < 0)
                {
                    _february = Math.Abs(value);
                }
            }
        }
        public int March
        {
            get { return _march; }
            set
            {
                try
                {
                    _march = value;
                }
                catch (FormatException)
                {
                    _march = default(int);
                }
                if (value < 0)
                {
                    _march = Math.Abs(value);
                }
            }
        }
        public int April
        {
            get { return _april; }
            set
            {
                try
                {
                    _april = value;
                }
                catch (FormatException)
                {
                    _april = default(int);
                }
                if (value < 0)
                {
                    _april = Math.Abs(value);
                }
            }
        }
        public int May
        {
            get { return _may; }
            set
            {
                try
                {
                    _may = value;
                }
                catch (FormatException)
                {
                    _may = default(int);
                }
                if (value < 0)
                {
                    _may = Math.Abs(value);
                }
            }
        }
        public int June
        {
            get { return _june; }
            set
            {
                try
                {
                    _june = value;
                }
                catch (FormatException)
                {
                    _june = default(int);
                }
                if (value < 0)
                {
                    _june = Math.Abs(value);
                }
            }
        }
        public int July
        {
            get { return _july; }
            set
            {
                try
                {
                    _july = value;
                }
                catch (FormatException)
                {
                    _july = default(int);
                }
                if (value < 0)
                {
                    _july = Math.Abs(value);
                }
            }
        }
        public int August
        {
            get { return _august; }
            set
            {
                try
                {
                    _august = value;
                }
                catch (FormatException)
                {
                    _august = default(int);
                }
                if (value < 0)
                {
                    _august = Math.Abs(value);
                }
            }
        }
        public int September
        {
            get { return _september; }
            set
            {
                try
                {
                    _september = value;
                }
                catch (FormatException)
                {
                    _september = default(int);
                }
                if (value < 0)
                {
                    _september = Math.Abs(value);
                }
            }
        }
        public int October
        {
            get { return _october; }
            set
            {
                try
                {
                    _october = value;
                }
                catch (FormatException)
                {
                    _october = default(int);
                }
                if (value < 0)
                {
                    _october = Math.Abs(value);
                }
            }
        }
        public int November
        {
            get { return _november; }
            set
            {
                try
                {
                    _november = value;
                }
                catch (FormatException)
                {
                    _november = default(int);
                }
                if (value < 0)
                {
                    _november = Math.Abs(value);
                }
            }
        }
        public int December
        {
            get { return _december; }
            set
            {
                try
                {
                    _december = value;
                }
                catch (FormatException)
                {
                    _december = default(int);
                }
                if (value < 0)
                {
                    _december = Math.Abs(value);
                }
            }
        }
        public string GetName()
        {
                return _deviceName + ", "
                        + ("Зав. № " + _serialNumber) + ", "
                        + (_accuracy + "кл. т.") + ", "
                        + ("н. пр." + _lowLim) + ", "
                        + ("в. пр." + _upLim);
        }
    }
}