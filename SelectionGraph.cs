﻿using System;

namespace Coursework
{
    class SelectionGraph
    {
        string _selName;
        int _calibYear;
        int _period;
        int _count;
        int _areaCode;

        public string SelName//Наименование
        {
            get
            {
                return _selName;
            }
            set
            {
                _selName = value;
            }
        }
        public int CodeArea//Код области измерений
        {
            get { return _areaCode; }
            set
            {
                try
                {
                    _areaCode = value;
                }
                catch (FormatException)
                {
                    _areaCode = default(int);
                }
                if (value < 0)
                {
                    _areaCode = Math.Abs(value);
                }
            }
        }
        public int CalibYear//Год поверки
        {
            get { return _calibYear; }
            set
            {
                try
                {
                    _calibYear = value;
                }
                catch (FormatException)
                {
                    _calibYear = default(int);
                }
                if (value < 0)
                {
                    _calibYear = Math.Abs(value);
                }
            }
        }
        public int SelPeriod//Периодичность
        {
            get { return _period; }
            set
            {
                try
                {
                    _period = value;
                }
                catch (FormatException)
                {
                    _period = default(int);
                }
                if (value < 0)
                {
                    _period = Math.Abs(value);
                }
            }
        }
        public int CountOfDevicesPerMonth//Кол-во приборов за выбранный месяц
        {
            get { return _count; }
            set
            {
                try
                {
                    _count = value;
                }
                catch (FormatException)
                {
                    _count = default(int);
                }
                if (value < 0)
                {
                    _count = Math.Abs(value);
                }
            }
        }
    }
}
